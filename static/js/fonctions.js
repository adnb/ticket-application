function changeFin() {
    date = new Date();
    var jour = date.getDate()+"";
    while (jour.length < 2) jour = "0" + jour;
    var mois = (date.getMonth()+1)+"";
    while (mois.length < 2) mois = "0" + mois;
    var annee = date.getFullYear()+"";
    var heure = date.getHours()+"";
    while (heure.length < 2) heure = "0" + heure;
    var minute = date.getMinutes()+"";
    while (minute.length < 2) minute = "0" + minute;
    var seconde = date.getSeconds()+"";
    while (seconde.length < 2) seconde = "0" + seconde;
    document.getElementById('id_fin').value = jour+'/'+mois+'/'+annee+' '+heure+':'+minute+':'+seconde;
    return false;
}
function changeDebut() {
    date = new Date();
    var jour = date.getDate()+"";
    while (jour.length < 2) jour = "0" + jour;
    var mois = (date.getMonth()+1)+"";
    while (mois.length < 2) mois = "0" + mois;
    var annee = date.getFullYear()+"";
    var heure = date.getHours()+"";
    while (heure.length < 2) heure = "0" + heure;
    var minute = date.getMinutes()+"";
    while (minute.length < 2) minute = "0" + minute;
    var seconde = date.getSeconds()+"";
    while (seconde.length < 2) seconde = "0" + seconde;
    document.getElementById('id_debut').value = jour+'/'+mois+'/'+annee+' '+heure+':'+minute+':'+seconde;
    return false;
}
