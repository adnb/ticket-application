# Ticket Application

A simple Django application to track activity.

You can create categories, projects, tasks, and have a pretty dashboard to see the time evolution of your projects.

Pages includes:

## List of tasks
![Historique](doc/django-histo.png)

## Chart view
![Graphe](doc/django-evolution.png)

## Add object interface
![Add task](doc/django-tache.png)

# Installation

```
# Optional definition of a virtual environment
pip install virtualenv
pip -m virtualenv venv
source venv/bin/activate

pip install Django
# Start the "ticket" project
django-admin startProject ticket
cd ticket
# Start the "app" application
python manage.py startapp app
```

# Run

```
# Optional use of a virtual environment
source venv/bin/activate

python manage.py runserver MY.IP:8080
```

# Disclaimer

The chart page uses [CanvasJS](http://canvasjs.com). It was released as [CC](http://creativecommons.org/licenses/by-nc/3.0/deed.en_US) for personal use and it needs to be licensed under commercial use -- see terms [here](http://canvasjs.com/license-canvasjs/).
