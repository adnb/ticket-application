from django import template

register = template.Library()

@register.filter
def hour(value):
    if value < 60:
      return str(value)+" min."
    else:
      if value % 60 < 10:
        return str(value // 60) + "h0"+ str(value % 60)
      else:
        return str(value // 60) + "h"+ str(value % 60)
