# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Categorie(models.Model):

    nom = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

    class Meta:
        ordering = ['nom']

    def __unicode__(self):
        return unicode(self.nom)

class Statut(models.Model):

    nom = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

    class Meta:
        ordering = ['nom']

    def __unicode__(self):
        return unicode(self.nom)

class Projet(models.Model):

    nom = models.CharField(max_length=255, verbose_name=u'Nom')
    categorie = models.ForeignKey('Categorie', on_delete=models.CASCADE, null=True, blank=True, verbose_name=u'Catégorie')
    statut = models.ForeignKey('Statut', default=0, verbose_name=u'Statut', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True, verbose_name=u'Description')
    debut = models.DateField(default=timezone.now, verbose_name=u'Date de début')
    fin = models.DateField(verbose_name=u'Date de fin', null=True, blank=True)
    creeLe = models.DateTimeField(auto_now_add=True, verbose_name=u'Date de création')
    misAJourLe = models.DateTimeField(auto_now=True, verbose_name=u'Date de mise à jour')

    @classmethod
    def all(self):
        return self

    def __str__(self):
        return self.nom

    def __unicode__(self):
        return unicode(self.nom)

    class Meta:
        ordering = ['nom']

class Tache(models.Model):

    projet = models.ForeignKey('Projet', on_delete=models.CASCADE, verbose_name=u'Projet')
    note = models.TextField(null=True, blank=True, verbose_name=u'Note')
    debut = models.DateTimeField(default=timezone.now, verbose_name=u'Date de début', null=True, blank=True)
    fin = models.DateTimeField(default=timezone.now, verbose_name='Date de fin', null=True, blank=True)
    temps = models.IntegerField(null=True, blank=True, verbose_name=u'Temps en minutes')
    score = models.IntegerField(null=True, blank=True, verbose_name=u'Score')
    utilisateur = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    creeLe = models.DateTimeField(auto_now_add=True, verbose_name=u'Date de création')
    misAJourLe = models.DateTimeField(auto_now=True, verbose_name=u'Date de mise à jour')
