# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from . import models
from . import forms

from datetime import date as dt
from datetime import timedelta

from .fonctions import filtreTacheParDate, traitementPost

tempsMax = 35

# Create your views here.

def index(request):
    """
    Page d'index
    """
    valide = traitementPost(request)
    projets = models.Projet.objects.filter(statut__nom=u"En cours").order_by("categorie","nom")
    projetsNouveaux = models.Projet.objects.filter(statut__nom=u"Nouveau").order_by("categorie")
    return render(request, "app/index.html", locals())

def histo(request):
    """
    Page listant les tâches
    """
    valide = traitementPost(request)
    page = 1
    categorieFiltre = ""
    projetFiltre = ""
    jourFiltre = ""
    searchFiltre = ""
    if request.method == "GET":
        try:
            searchFiltre = request.GET.get("search","")
        except:
            pass
        try:
            page = int(request.GET.get("page","1"))
        except:
            pass
        try:
            categorieFiltre = request.GET.get("categorie","")
        except:
            pass
        try:
            projetFiltre = request.GET.get("projet","")
        except:
            pass
        try:
            jourFiltre = request.GET.get("jour","")
        except:
            pass

    categorieStr = ""
    if categorieFiltre != "":
        categorieStr = "&categorie="+categorieFiltre
    projetStr = ""
    if projetFiltre != "":
        projetStr = "&projet="+projetFiltre
    jourStr = ""
    if jourFiltre != "":
        jourStr = "&jour="+jourFiltre
    searchStr = ""
    if searchFiltre != "":
        searchStr = "&search="+searchFiltre

    if projetFiltre != "" and categorieFiltre != "":
        if jourFiltre != "":
            jour = [int(elt) for elt in jourFiltre.split("/")]
            jour[2] = int("20"+str(jour[2]))
            taches = models.Tache.objects.filter(projet__categorie__nom=categorieFiltre).filter(projet__nom=projetFiltre).filter(debut__range=[dt(jour[2],jour[1],jour[0]),dt(jour[2],jour[1],jour[0])+timedelta(days=1)])
        else:
            taches = models.Tache.objects.filter(projet__categorie__nom=categorieFiltre).filter(projet__nom=projetFiltre)
        pageMax = len(taches)//10 + 1
        taches = taches.order_by("-debut")[(page-1)*10:page*10]
    elif projetFiltre != "":
        if jourFiltre != "":
            jour = [int(elt) for elt in jourFiltre.split("/")]
            jour[2] = int("20"+str(jour[2]))
            taches = models.Tache.objects.filter(projet__nom=projetFiltre).filter(debut__range=[dt(jour[2],jour[1],jour[0]),dt(jour[2],jour[1],jour[0])+timedelta(days=1)])
        else:
            taches = models.Tache.objects.filter(projet__nom=projetFiltre)
        pageMax = len(taches)//10 + 1
        taches = taches.order_by("-debut")[(page-1)*10:page*10]
    elif categorieFiltre != "":
        if jourFiltre != "":
            jour = [int(elt) for elt in jourFiltre.split("/")]
            jour[2] = int("20"+str(jour[2]))
            taches = models.Tache.objects.filter(projet__categorie__nom=categorieFiltre).filter(debut__range=[dt(jour[2],jour[1],jour[0]),dt(jour[2],jour[1],jour[0])+timedelta(days=1)])
        else:
            taches = models.Tache.objects.filter(projet__categorie__nom=categorieFiltre)
        pageMax = len(taches)//10 + 1
        taches = taches.order_by("-debut")[(page-1)*10:page*10]
    else:
        if jourFiltre != "":
            jour = [int(elt) for elt in jourFiltre.split("/")]
            jour[2] = int("20"+str(jour[2]))
            taches = models.Tache.objects.filter(debut__range=[dt(jour[2],jour[1],jour[0]),dt(jour[2],jour[1],jour[0])+timedelta(days=1)])
        elif searchFiltre != "":
            taches = models.Tache.objects.filter(note__contains=searchFiltre)
        else:
            taches = models.Tache.objects.all()
        pageMax = len(taches)//10 + 1
        taches = taches.order_by("-debut")[(page-1)*10:page*10]

    precedent = page -1
    suivant = page +1

    return render(request, "app/histo.html", locals())

def projets(request):
    """
    Page listant les projets
    """
    valide = traitementPost(request)
    page = 1
    categorieFiltre = ""
    statutFiltre = ""
    if request.method == "GET":
        try:
            page = int(request.GET.get("page","1"))
        except:
            pass
        try:
            categorieFiltre = request.GET.get("categorie","")
        except:
            pass
        try:
            statutFiltre = request.GET.get("statut","")
        except:
            pass

    if statutFiltre != "":
        projets = models.Projet.objects.filter(statut__nom=statutFiltre)
        pageMax = len(projets)//10 + 1
        projets = projets.order_by("-debut")[(page-1)*10:page*10]
    elif categorieFiltre != "":
        projets = models.Projet.objects.filter(categorie__nom=categorieFiltre)
        pageMax = len(projets)//10 + 1
        projets = projets.order_by("-debut")[(page-1)*10:page*10]
    else:
        projets = models.Projet.objects.all()
        pageMax = len(projets)//10 + 1
        projets = projets.order_by("-debut")[(page-1)*10:page*10]

    precedent = page -1
    suivant = page +1

    return render(request, "app/projets.html", locals())

def projet(request, id):
    """
    Page de présentation d'un projet, avec graphe du temps passé et les tâches associées
    """
    if request.method == "GET":
        try:
            duree = int(request.GET.get("duree",tempsMax))
        except:
            pass
        try:
            decalage = int(request.GET.get("decalage","0"))-1
        except:
            pass

    today = dt.today()
    valide = traitementPost(request)
    projet = models.Projet.objects.get(id=id)

    page = 1
    if request.method == "GET":
        try:
            page = int(request.GET.get("page","1"))
        except:
            pass

    taches = models.Tache.objects.filter(projet=id)
    pageMax = len(taches)//10 + 1
    taches = taches.order_by("-debut")[(page-1)*10:page*10]

    precedent = page -1
    suivant = page +1

    objets = models.Tache.objects.filter(projet__id=id)
    tempsTotal = 0
    for objet in objets:
        tempsTotal += objet.temps

    evolutionProjet = list()
    objets = models.Tache.objects.filter(projet__id=id).filter(debut__range=[today-timedelta(days=duree+decalage), today-timedelta(days=decalage)])
    dico = dict()
    for objet in objets:
        date = objet.debut.strftime("%d/%m/%y")
        try:
            if objet.temps != None:
                if date in dico.keys():
                    dico[date] += objet.temps
                else:
                    dico[date] = objet.temps
        except:
            pass
    dateRange = [today - timedelta(days=duree+decalage-x) for x in range(0, duree)]
    for date in dateRange:
        try:
            evolutionProjet.append((date.strftime("%d/%m/%y"), dico[date.strftime("%d/%m/%y")]))
        except:
            evolutionProjet.append((date.strftime("%d/%m/%y"), 0))

    return render(request, "app/projet.html", locals())

def ajouteProjet(request):
    """
    Page d'ajout d'un projet
    """
    form = forms.projetForm(request.POST or None)
    type = u"Ajout"
    modele = u"Projet"
    redirection = "projets"
    return render(request, "app/form.html", locals())

def modifieProjet(request, id):
    """
    Page d'édition d'un projet
    """
    enregistrement = models.Projet.objects.get(id=id)
    form = forms.projetForm(request.POST or None, instance=enregistrement)
    type = u"Modification"
    modele = u"Projet"
    redirection = "projets"
    return render(request, "app/form.html", locals())

def supprimeProjet(request, id):
    """
    Page de suppression d'un projet
    """
    enregistrement = models.Projet.objects.get(id=id)
    form = forms.projetForm(request.POST or None, instance=enregistrement)
    type = u"Suppression"
    modele = u"Projet"
    redirection = "projets"
    return render(request, "app/form.html", locals())

def ajouteTache(request, idProjet):
    """
    Page d'ajout d'une tâche
    """
    derniere_tache = models.Tache.objects.order_by("-fin").first()
    dernier_fin = getattr(derniere_tache, "fin")
    data = {"projet": idProjet, "debut": dernier_fin}
    form = forms.tacheForm(request.POST or None, initial=data)
    type = u"Ajout"
    modele = u"Tâche"
    redirection = "histo"
    return render(request, "app/form.html", locals())

def modifieTache(request, id):
    """
    Page d'édition d'une tâche
    """
    enregistrement = models.Tache.objects.get(id=id)
    form = forms.tacheForm(request.POST or None, instance=enregistrement)
    type = u"Modification"
    modele = u"Tâche"
    redirection = "histo"
    categorie = enregistrement.projet.categorie.id
    idProjet = enregistrement.projet.id
    return render(request, "app/form.html", locals())

def supprimeTache(request, id):
    """
    Page de suppression d'une tâche
    """
    enregistrement = models.Tache.objects.get(id=id)
    form = forms.tacheForm(request.POST or None, instance=enregistrement)
    type = u"Suppression"
    modele = u"Tâche"
    redirection = "projet"
    idProjet = enregistrement.projet.id
    return render(request, "app/form.html", locals())

def categorie(request):
    """
    Page listant les catégories
    """
    valide = traitementPost(request)
    categories = models.Categorie.objects.all()
    return render(request, "app/categorie.html", locals())

def ajouteCategorie(request):
    """
    Page d'ajout d'une catégorie
    """
    form = forms.categorieForm(request.POST or None)
    type = u"Ajout"
    modele = u"Catégorie"
    redirection = "categorie"
    return render(request, "app/form.html", locals())

def modifieCategorie(request, id):
    """
    Page d'édition d'une catégorie
    """
    enregistrement = models.Categorie.objects.get(id=id)
    form = forms.categorieForm(request.POST or None, instance=enregistrement)
    type = u"Modification"
    modele = u"Catégorie"
    redirection = "categorie"
    return render(request, "app/form.html", locals())

def supprimeCategorie(request, id):
    """
    Page de suppression d'une catégorie
    """
    enregistrement = models.Categorie.objects.get(id=id)
    form = forms.categorieForm(request.POST or None, instance=enregistrement)
    type = u"Suppression"
    modele = u"Catégorie"
    redirection = "categorie"
    return render(request, "app/form.html", locals())

#@login_required
def graphes(request):
    """
    Page des graphes
    Calcule l'évolution du temps passé par catéorie, la répartition des catégories, des projets et l'évolution du score
    """
    if request.method == "GET":
        try:
            duree = int(request.GET.get("duree",tempsMax))
        except:
            pass
        try:
            decalage = int(request.GET.get("decalage","0"))-1
        except:
            pass
        semaine = "semaine" in request.GET
        pourcent = "pourcent" in request.GET

    pourcentStr = ""
    if pourcent:
        pourcentStr = "&pourcent"

    today = dt.today()
    if semaine:
        strftime = "s%W a%y"
        dateRange = [today - timedelta(days=duree+decalage-x*7) for x in range(0, duree//7)]
        semaineStr = "&semaine"
    else:
        strftime = "%a %d/%m/%y"
        dateRange = [today - timedelta(days=duree+decalage-x) for x in range(0, duree)]
        semaineStr = ""

    valide = traitementPost(request)

    categories = models.Categorie.objects.all()
    projets = models.Projet.objects.all()

    # Evolution du temps passe par categorie
    evolutionTemps = list()
    for categorie in categories:
        dico = dict()
        dico["categorie"] = categorie.nom
        objets = models.Tache.objects.filter(projet__categorie__id=categorie.id).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
        dico["dict"] = dict()
        zero = True
        for objet in objets:
            if objet.temps > 0:
                zero = False
            date = objet.debut.strftime(strftime)
            if date in dico["dict"].keys():
                dico["dict"][date] += objet.temps
            else:
                dico["dict"][date] = objet.temps
        if zero:
            continue
        dico["list"] = list()
        # Pour remplir les trous
        for date in dateRange:
            try:
                temps = dico["dict"][date.strftime(strftime)]
                if temps > 1439:
                    tempsStr = "{}j{:0>2}h{:0>2}".format(str(temps//60//24),str((temps%1440)//60),str((temps%1440)%60))
                elif temps > 59:
                    tempsStr = "{}h{:0>2}".format(str(temps//60),str(temps%60))
                else:
                    tempsStr = str(temps)+" min."
                dico["list"].append((date.strftime(strftime), temps, tempsStr))
            except:
                dico["list"].append((date.strftime(strftime), 0, "0 min."))
        evolutionTemps.append(dico)

    # Doughnut du temps passe par categorie
    tempsTotal = 0
    repartitionCategorie = list()
    for categorie in categories:
        objets = models.Tache.objects.filter(projet__categorie__id=categorie.id).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
        pile = 0
        for objet in objets:
            tempsTotal += objet.temps
            pile += objet.temps
        if pile > 1439:
            tempsStr = "{}j{:0>2}h{:0>2}".format(str(pile//60//24),str((pile%1440)//60),str((pile%1440)%60))
        elif pile > 59:
            tempsStr = "{}h{:0>2}".format(str(pile//60),str(pile%60))
        else:
            tempsStr = str(pile)+" min."
        repartitionCategorie.append((categorie.nom, pile, tempsStr))
    repartitionCategorie = sorted([(cle, valeur, tempsStr) for (cle, valeur, tempsStr) in repartitionCategorie if valeur > 0], key=lambda tup:tup[1])

    # Doughnut du temps passe par projet
    repartitionProjet = list()
    for projet in projets:
        objets = models.Tache.objects.filter(projet__id=projet.id).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
        pile = 0
        for objet in objets:
            pile += objet.temps
        if pile > 1439:
            tempsStr = "{}j{:0>2}h{:0>2}".format(str(pile//60//24),str((pile%1440)//60),str((pile%1440)%60))
        elif pile > 59:
            tempsStr = "{}h{:0>2}".format(str(pile//60),str(pile%60))
        else:
            tempsStr = str(pile)+" min."
        repartitionProjet.append((projet.categorie.nom+": "+projet.nom,pile,tempsStr))
    repartitionProjet = sorted([(cle, valeur, tempsStr) for (cle, valeur, tempsStr) in repartitionProjet if valeur > 0], key=lambda tup:tup[1])

    # Doughnut du temps passe par projet pour une catégorie
    repartitionPourCategorie = list()
    for categorie in categories:
        elt = list()
        projets = models.Projet.objects.filter(categorie__id=categorie.id)
        for projet in projets:
            objets = models.Tache.objects.filter(projet__id=projet.id).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
            pile = 0
            for objet in objets:
                pile += objet.temps
            if pile > 1439:
                tempsStr = "{}j{:0>2}h{:0>2}".format(str(pile//60//24),str((pile%1440)//60),str((pile%1440)%60))
            elif pile > 59:
                tempsStr = "{}h{:0>2}".format(str(pile//60),str(pile%60))
            else:
                tempsStr = str(pile)+" min."
            elt.append((projet.nom,pile,tempsStr))
        elt = sorted([(cle, valeur, tempsStr) for (cle, valeur, tempsStr) in elt if valeur > 0], key=lambda tup:tup[1])
        repartitionPourCategorie.append({"categorie":categorie.nom,"list":elt})

    # Evolution du score
    evolutionScore = list()
    objets = models.Tache.objects.filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
    dico = dict()
    for objet in objets:
        date = objet.debut.strftime(strftime)
        try:
            if objet.score != None:
                if date in dico.keys():
                    dico[date] += objet.score
                else:
                    dico[date] = objet.score
        except:
            pass
    # Pour remplir les trous
    for date in dateRange:
        try:
            evolutionScore.append((date.strftime(strftime), dico[date.strftime(strftime)]))
        except:
            evolutionScore.append((date.strftime(strftime), 0))

    decalage = decalage + 1
    return render(request, "app/graphes.html", locals())

def ajaxProjet(request):
    """
    Calcule le graphe d'évolution du temps passé pour un projet donné
    """
    if request.method == "GET":
        try:
            duree = int(request.GET.get("duree",tempsMax))
        except:
            pass
        try:
            decalage = int(request.GET.get("decalage","0"))-1
        except:
            pass
        try:
            categorieNom = request.GET.get("categorie","")
        except:
            pass
        try:
            projetNom = request.GET.get("projet","")
        except:
            pass
        semaine = "semaine" in request.GET

    today = dt.today()
    if semaine:
        strftime = "s%W a%y"
        dateRange = [today - timedelta(days=duree+decalage-x*7) for x in range(0,duree//7)]
    else:
        strftime = "%a %d/%m/%y"
        dateRange = [today - timedelta(days=duree+decalage-x) for x in range(0,duree)]

    valide = traitementPost(request)

    objets = models.Tache.objects.filter(projet__categorie__nom=categorieNom).filter(projet__nom=projetNom)

    tempsTotal = 0
    for objet in objets:
        tempsTotal += objet.temps

    evolutionProjet = list()
    objets = models.Tache.objects.filter(projet__categorie__nom=categorieNom).filter(projet__nom=projetNom).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
    dico = dict()
    for objet in objets:
        date = objet.debut.strftime(strftime)
        try:
            if objet.temps != None:
                if date in dico.keys():
                    dico[date] += objet.temps
                else:
                    dico[date] = objet.temps
        except:
            pass
    # Pour remplir les trous
    for date in dateRange:
        try:
            temps = dico[date.strftime(strftime)]
            if temps > 1439:
                tempsStr = "{}j{:0>2}h{:0>2}".format(str(temps//60//24),str((temps%1440)//60),str((temps%1440)%60))
            elif temps > 59:
                tempsStr = "{}h{:0>2}".format(str(temps//60),str(temps%60))
            else:
                tempsStr = str(temps)+" min."
            evolutionProjet.append({"label":date.strftime(strftime), "y":temps, "str": tempsStr, "link" : "histo"})
        except:
            evolutionProjet.append({"label":date.strftime(strftime), "y":0, "str": "0 min."})

    data = {"liste":evolutionProjet}
    return JsonResponse(data)

def ajaxCategorie(request):
    """
    Calcule le graphe d'évolution du temps passé par projet pour une catégorie donnée
    """
    if request.method == "GET":
        try:
            duree = int(request.GET.get("duree",tempsMax))
        except:
            pass
        try:
            decalage = int(request.GET.get("decalage","0"))-1
        except:
            pass
        try:
            categorieNom = request.GET.get("categorie","")
        except:
            pass
        pourcent = "pourcent" in request.GET
        semaine = "semaine" in request.GET

    today = dt.today()
    if semaine:
        strftime = "s%W a%y"
        dateRange = [today - timedelta(days=duree+decalage-x*7) for x in range(0,duree//7)]
    else:
        strftime = "%a %d/%m/%y"
        dateRange = [today - timedelta(days=duree+decalage-x) for x in range(0,duree)]

    valide = traitementPost(request)

    evolutionCategorie = list()
    projets = models.Projet.objects.filter(categorie__nom=categorieNom).exclude(statut__nom=u"Nouveau")
    for projet in projets:
        dico = dict()
        if pourcent:
            dico["type"] = "stackedColumn100"
            dico["toolTipContent"] = "{name}: #percent% - {label}"
        else:
            dico["type"] = "stackedColumn"
            dico["toolTipContent"] = "{name}: {str} - {label}"
        dico["cursor"] = "pointer"
        dico["legendMarkerType"] = "circle"
        dico["name"] = projet.nom
        dico["showInLegend"] = True
        dico["dataPoints"] = list()

        objets = models.Tache.objects.filter(projet__id=projet.id).filter(debut__range=[today-timedelta(days=duree+decalage),today-timedelta(days=decalage)])
        dico["dict"] = dict()
        zero = True
        for objet in objets:
            if objet.temps > 0:
                zero = False
            date = objet.debut.strftime(strftime)
            if date in dico["dict"].keys():
                dico["dict"][date] += objet.temps
            else:
                dico["dict"][date] = objet.temps
        if zero:
            continue

        for date in dateRange:
            try:
                temps = dico["dict"][date.strftime(strftime)]
                if temps > 1439:
                    tempsStr = "{}j{:0>2}h{:0>2}".format(str(temps//60//24),str((temps%1440)//60),str((temps%1440)%60))
                elif temps > 59:
                    tempsStr = "{}h{:0>2}".format(str(temps//60),str(temps%60))
                else:
                    tempsStr = str(temps)+" min."
                dico["dataPoints"].append({"label":date.strftime(strftime), "y":temps, "str": tempsStr, "name":projet.nom})
            except:
                dico["dataPoints"].append({"label":date.strftime(strftime), "y":0, "str": "0 min.", "name":projet.nom})
        evolutionCategorie.append(dico)

    data = {"liste":evolutionCategorie}
    return JsonResponse(data)

def ajaxGetProjets(request):
    """
    Récupère la liste de projets pour une catégorie pour filtrer les formulaires
    """
    if request.method == "GET":
        try:
            categorie = int(request.GET.get("categorie", ""))
        except:
            pass

    projets = models.Projet.objects.filter(categorie__id=categorie).filter(statut__nom=u"En cours").order_by("nom")

    data = dict()
    data["liste"] = list()
    for projet in projets:
        data["liste"].append((projet.id, projet.nom))
    return JsonResponse(data)
