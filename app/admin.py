from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Projet)
admin.site.register(models.Tache)
admin.site.register(models.Categorie)
admin.site.register(models.Statut)
