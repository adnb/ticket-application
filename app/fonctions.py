# -*- coding: utf-8 -*-
from . import models
from . import forms

from datetime import date
today = date.today()
from datetime import timedelta

def filtreTacheParDate(date):
    return models.Tache.objects.filter(debut__range=[date-timedelta(days=1), date+timedelta(days=1)])

def traitementPost(request):
    valide = False
    phrase = u""
    if request.method == "POST":

        if request.POST.get('type','') == u'Suppression':
            if request.POST.get('modele','') == u'Projet':
                models.Projet.objects.get(id=request.POST.get('id','0')).delete()
                valide = True
                phrase = u'La suppression du projet a bien eu lieu.'

            if request.POST.get('modele','') == u'Tâche':
                models.Tache.objects.get(id=request.POST.get('id','0')).delete()
                valide = True
                phrase = u'La suppression de la tâche a bien eu lieu.'

            if request.POST.get('modele','') == u'Catégorie':
                models.Categorie.objects.get(id=request.POST.get('id','0')).delete()
                valide = True
                phrase = u'La suppression de la catégorie a bien eu lieu.'

        if request.POST.get('type','') == u'Ajout':
            if request.POST.get('modele','') == u'Projet':
                form = forms.projetForm(request.POST or None)
            if request.POST.get('modele','') == u'Tâche':
                form = forms.tacheForm(request.POST or None)
            if request.POST.get('modele','') == u'Catégorie':
                form = forms.categorieForm(request.POST or None)
            if form.is_valid():
                #nom = form.cleaned_data['nom']
                if request.POST.get('modele','') == u'Tâche':
                    instance = form.save(commit=False)
                    instance.temps = (instance.fin - instance.debut).seconds / 60
                    if request.user.is_authenticated:
                        instance.utilisateur = request.user
                form.save()
                valide = True
                phrase = u'La création a bien eu lieu.'

        if request.POST.get('type','')    == u'Modification':
            if request.POST.get('modele','') == u'Projet':
                enregistrement = models.Projet.objects.get(id=request.POST.get('id','0'))
                form = forms.projetForm(request.POST or None, instance=enregistrement)
            if request.POST.get('modele','') == u'Tâche':
                enregistrement = models.Tache.objects.get(id=request.POST.get('id','0'))
                form = forms.tacheForm(request.POST or None, instance=enregistrement)
            if request.POST.get('modele','') == u'Catégorie':
                enregistrement = models.Categorie.objects.get(id=request.POST.get('id','0'))
                form = forms.categorieForm(request.POST or None, instance=enregistrement)
            if form.is_valid():
                #nom = form.cleaned_data['nom']
                if request.POST.get('modele','') == u'Tâche':
                    instance = form.save(commit=False)
                    duree = (instance.fin - instance.debut).seconds
                    instance.temps = ((instance.fin - instance.debut).seconds) / 60
                form.save()
                valide = True
                phrase = u"La modification a bien eu lieu."

    return phrase

