# -*- coding: utf-8 -*-
from django import forms
from . import models

class projetForm(forms.ModelForm):
    class Meta:
        model = models.Projet
        fields = '__all__'

class tacheForm(forms.ModelForm):
    projet = forms.ModelChoiceField(queryset=models.Projet.objects.all())
    categorie = forms.ModelChoiceField(queryset=models.Categorie.objects.all())
    class Meta:
        model = models.Tache
        fields = ['categorie','projet','note','debut','fin','score']

class categorieForm(forms.ModelForm):
    class Meta:
        model = models.Categorie
        fields = '__all__'
