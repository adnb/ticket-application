# -*- coding: utf-8 -*-
from django.urls import re_path, include

from . import views

urlpatterns = [
        re_path(r'^$', views.index, name='index'),
        re_path(r'^histo$', views.histo, name='histo'),
        re_path(r'^projets$', views.projets, name='projets'),
        re_path(r'^projet/ajoute$', views.ajouteProjet, name='ajouteProjet'),
        re_path(r'^projet/(?P<id>[0-9]+)$', views.projet, name='projet'),
        re_path(r'^projet/(?P<id>[0-9]+)/modifie$', views.modifieProjet, name='modifieProjet'),
        re_path(r'^tache/(?P<idProjet>[0-9]+)/ajoute$', views.ajouteTache, name='ajouteTache'),
        re_path(r'^tache/(?P<id>[0-9]+)/modifie$', views.modifieTache, name='modifieTache'),
        re_path(r'^categorie$', views.categorie, name='categorie'),
        re_path(r'^categorie/ajoute$', views.ajouteCategorie, name='ajouteCategorie'),
        re_path(r'^categorie/(?P<id>[0-9]+)/modifie$', views.modifieCategorie, name='modifieCategorie'),
        re_path(r'^graphes$', views.graphes, name='graphes'),
        re_path(r'^ajaxProjet$', views.ajaxProjet, name='ajaxProjet'),
        re_path(r'^ajaxCategorie$', views.ajaxCategorie, name='ajaxCategorie'),
        re_path(r'^ajaxGetProjets$', views.ajaxGetProjets, name='ajaxGetProjets'),
]
